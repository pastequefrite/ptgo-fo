import React, { useState } from 'react';
import useSound from 'use-sound';
import multiplexFx from './sounds/multiplex.mp3';

import moment from 'moment';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import logo from './logo.svg';
import './App.css';


const URL = "https://ptgo.ddns.net:8443/viewer-count";
const TIMER = 60;

let loopCall;

function App() {

  const [res, setRes] = useState({});
  const [disabled, setDisabled] = useState(true);
  const [callDate, setCallDate] = useState();

  const [play] = useSound(multiplexFx);


  const refreshViewerCount = async () => {
    const res = await fetch(URL);
    return await res.json();
  };

  const autoRefresh = () => {
    setDisabled(false);

    // call API
    loopCall = setInterval(async () => {
      refreshViewerCount()
        .then(resp => {
          setRes(resp.response);
          if (resp.response.info.includes("GOAL UNLOCKED !!!")) {
            play();
          }
          toast.success(resp.response.info);
        })
        .catch(error => {
          toast.error("Error : " + error)
        })
        .finally(() => {
          setCallDate(moment().format('LTS'));
        })

      console.log("Refreshed");
    }, 1000 * TIMER);

  };

  const stopAutoRefresh = () => {
    // stop API
    clearInterval(loopCall);
    setDisabled(true);
    console.log("Stopped");
  };

  return (
    <div className="App">
      <header className="App-header">
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={true}
          newestOnTop={false}
          closeOnClick={false}
          rtl={false}
          draggable={false}
          pauseOnHover={false}
          pauseOnFocusLoss={false} />
        <img src={logo} className="App-logo" alt="logo" />
        <p>Before launching auto-refresh, pls click on this <a target="_blank" rel="noreferrer" href='https://ptgo.ddns.net:8443'>link</a> to ensure you're connected.</p>
        <p>Last call : {callDate}</p>
        <p>Infos : {res.info}</p>
        <p>Viewers : {res.viewers}</p>
        <button onClick={autoRefresh}>Auto Refresh</button>
        <button onClick={stopAutoRefresh} disabled={disabled}>Stop</button>
      </header>
    </div>
  );
}

export default App;
